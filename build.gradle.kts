// Top-level build file where you can add configuration options common to all sub-projects/modules.

plugins {
    System.setProperty("kotlin_version","1.9.0")
    System.setProperty("core-ktx","1.9.0")
    id("com.android.application") version "8.1.1" apply false
    id("org.jetbrains.kotlin.android") version System.getProperty("kotlin_version") apply false
}
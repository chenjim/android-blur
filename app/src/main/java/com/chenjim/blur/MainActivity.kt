package com.chenjim.blur

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.renderscript.Toolkit
import kotlin.system.measureTimeMillis

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888
        options.inScaled = false
        val bmp = BitmapFactory.decodeResource(resources, R.drawable.data, options)

        Log.d(
            "BlurActivity", "FastBlur ${bmp.width}X${bmp.height} cost time: ${
                measureTimeMillis {
                    FastBlur.doBlur(bmp, 20, false)
                }
            }"
        )

        Log.d(
            "BlurActivity", "RSBlur ${bmp.width}X${bmp.height} cost time: ${
                measureTimeMillis {
                    RSBlur.blurBitmap(bmp, bmp, 20F, baseContext)
                }
            }"
        )

        Log.d(
            "BlurActivity", "Toolkit ${bmp.width}X${bmp.height} cost time: ${
                measureTimeMillis {
                    Toolkit.blur(bmp, 20, null)
                }
            }"
        )
    }
}